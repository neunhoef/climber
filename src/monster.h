// monster.h

#ifndef MONSTER_H
#define MONSTER_H

#include "media.h"

#include <chrono>

class Monster {
 public:
  bool sleeping;
  static constexpr double speed = 0.5;
  int spawnr;
  int spawnc;
  size_t shape;
  double x;
  double y;
  double vx;
  double vy;
  int want_vx;
  int want_vy;
  SDL_Rect pos;
  std::chrono::steady_clock::time_point sleepingUntil;

 public:
  Monster(int spawnr, int spawnc, size_t shape, int width, int height)
    : sleeping(false), spawnr(spawnr), spawnc(spawnc), shape(shape),
      x(spawnc * TILE_WIDTH), y(spawnr * TILE_HEIGHT),
      vx(0), vy(0), want_vx(0), want_vy(0) {
    pos = { static_cast<int>(floor(x + 0.5)), static_cast<int>(floor(y + 0.5)),
            width, height };
  }

  void respawn() {
    x = spawnc * TILE_WIDTH;
    y = spawnr * TILE_HEIGHT;
    vx = 0;
    vy = 0;
    want_vx = 0;
    want_vy = 0;
    pos.x = static_cast<int>(floor(x + 0.5));
    pos.y = static_cast<int>(floor(y + 0.5));
  }
 
  void gotoSleep(std::chrono::duration<long int> forSeconds) {
    sleeping = true;
    sleepingUntil = std::chrono::steady_clock::now() + forSeconds;
  }

  void move(Level& l);

  int getTileCol() {
    return (pos.x + (pos.w / 2)) / TILE_WIDTH;
  }

  int getTileRow() {
    return (pos.y + (pos.h / 2)) / TILE_HEIGHT;
  }

  void render(Media& media, SDL_Rect& camera) {
    media.getShape(shape)->render(camera, static_cast<int>(x),
                                          static_cast<int>(y));
  }
};

#endif
