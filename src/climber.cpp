// climber.cpp - main file

#include <iostream>
#include <string>
#include <sstream>
#include <chrono>
#include <thread>

#include "graphics.h"
#include "SDL2/SDL_mixer.h"
#include "level.h"
#include "bomb.h"
#include "media.h"
#include "monster.h"
#include "player.h"

#define micros(x) std::chrono::duration_cast<std::chrono::microseconds>(x).count()

static int constexpr STATE_PLAY = 0;
static int constexpr STATE_LOST = 1;
static int constexpr STATE_WON = 2;
static int constexpr STATE_RESTART = 3;
static int constexpr STATE_QUIT = 4;

bool usingJoystick = false;
SDL_Joystick* joystick = nullptr;

void pollEvents(Player& player, Level& l, Media& media,
                std::vector<Bomb>& bombs, int levelNr, int& gameState) {
  if (usingJoystick && joystick != nullptr) {
    int x = SDL_JoystickGetAxis(joystick, 0);
    int y = SDL_JoystickGetAxis(joystick, 1);
    //std::cout << "Joystick reading: " << x << "/" << y << "\n";
    if (x < -5000) {
      player.want_vx = -1;
    } else if (x > 5000) {
      player.want_vx = 1;
    } else {
      player.want_vx = 0;
    }
    if (y < -5000) {
      player.want_vy = -1;
    } else if (y > 5000) {
      player.want_vy = 1;
    } else {
      player.want_vy = 0;
    }
    if (player.want_vx != 0 && player.want_vx != 0) {
      if (abs(x) > abs(y)) {
        player.want_vy = 0;
      } else if (abs(x) < abs(y)) {
        player.want_vx = 0;
      } else {
        player.want_vx = 0;
        player.want_vy = 0;
      }
    }
  }
  static int lastKeyDown = -1;
  SDL_Event e;
  while (SDL_PollEvent(&e) != 0) {
    if (e.type == SDL_QUIT) {
      gameState = STATE_QUIT;
    }
    int posx = 0;
    int posy = 0;
    if (e.type == SDL_KEYDOWN && e.key.repeat == 0) {
      switch (e.key.keysym.sym) {
        case SDLK_ESCAPE: gameState = STATE_QUIT; break;
        case SDLK_UP:     player.want_vx = 0;  player.want_vy = -1; break;
        case SDLK_DOWN:   player.want_vx = 0;  player.want_vy = 1;  break;
        case SDLK_LEFT:   player.want_vx = -1; player.want_vy = 0;  break;
        case SDLK_RIGHT:  player.want_vx = 1;  player.want_vy = 0;  break;
        case SDLK_r:
          gameState = STATE_RESTART;
          break;
        case SDLK_SPACE:
          if (gameState == STATE_PLAY) {
            posx = (player.pos.x + PLAYER_WIDTH/2) / TILE_WIDTH;
            posy = (player.pos.y + PLAYER_HEIGHT/2) / TILE_HEIGHT;
            if (posx != player.lastBombx || posy != player.lastBomby) {
              bombs.emplace_back(posx, posy);
              player.lastBombx = posx;
              player.lastBomby = posy;
            }
          }
          break;
      }
      lastKeyDown = e.key.keysym.sym;
    } else if (e.type == SDL_KEYUP && e.key.repeat == 0) {
      if (e.key.keysym.sym == lastKeyDown) {
        player.want_vx = 0;
        player.want_vy = 0;
      }
    } else if (e.type == SDL_JOYBUTTONDOWN) {
      if (gameState == STATE_PLAY) {
        posx = (player.pos.x + PLAYER_WIDTH/2) / TILE_WIDTH;
        posy = (player.pos.y + PLAYER_HEIGHT/2) / TILE_HEIGHT;
        if (posx != player.lastBombx || posy != player.lastBomby) {
          bombs.emplace_back(posx, posy);
          player.lastBombx = posx;
          player.lastBomby = posy;
        }
      }
    }
  }
}

int main(int argc, char *argv[]) {
  std::cout << "Welcome to climber!" << std::endl;

  bool fullscreen = false;
  bool onlyKeyboard = false;
  int level = 1;
  for (int i = 0; i < argc; ++i) {
    if (strcmp(argv[i], "-f") == 0) {
      fullscreen = true;
    } else if (strcmp(argv[i], "-k") == 0) {
      onlyKeyboard = true;
    } else if (argv[i][0] >= '0' && argv[i][0] <= '9') {
      level = atoi(argv[i]);
    }
  }

  Media media;

  if (!media.init(fullscreen)) {
    std::cerr << "Initialization failed!" << std::endl;
    return 1;
  }

  usingJoystick = media.haveJoystick();

  if (onlyKeyboard) {
    usingJoystick = false;
  }

  if (usingJoystick) {
    joystick = SDL_JoystickOpen(0);
  }

  if (!media.loadMediaFromMemory()) {
    std::cerr << "Loading of media failed!" << std::endl;
    return 2;
  }

  int gameState = STATE_PLAY;

  do {
    Level l;

    if (!l.load(level)) {
      std::cerr << "Could not load level " << level << std::endl;
      return 3;
    }

    media.makeBackground(l);

    SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

    auto constexpr frameTime = std::chrono::milliseconds(5);
    Player player(l.startCol, l.startRow);
    std::vector<Level::Pos> golds = l.golds();   // Copy of gold blocks
    std::vector<Bomb> bombs;

    std::vector<Monster> monsters;
    size_t s = media.ShapeMonster;
    for (Level::Pos const& p : l.monsters) {
      Tile* t = media.getShape(s);
      monsters.emplace_back(p.r, p.c, s, t->getWidth(), t->getHeight());
      if (++s > media.ShapeMonster3) {
        s = media.ShapeMonster;
      }
    }

    l.computeDirToPlayer(player.getTileRow(), player.getTileCol(), true);

    gameState = STATE_PLAY;

    while (gameState == STATE_PLAY ||
           gameState == STATE_WON ||
           gameState == STATE_LOST) {
      auto frameStartTime = std::chrono::steady_clock::now();
      pollEvents(player, l, media, bombs, level, gameState);

      if (gameState == STATE_PLAY) {
        player.move(l);
        l.computeDirToPlayer(player.getTileRow(), player.getTileCol(), false);

        player.checkGold(golds, media);

        for (auto& m : monsters) {
          m.move(l);
        }

        // Check bombs:
        int playertilex = player.getTileCol();
        int playertiley = player.getTileRow();
        auto now = std::chrono::steady_clock::now();
        for (auto it = bombs.begin(); it != bombs.end(); ) {
          auto& b = *it;
          if (std::chrono::duration_cast<std::chrono::duration<double>>(
                now - b.startTime).count() > 1.25) {
            if (it->x == player.lastBombx && it->y == player.lastBomby) {
              player.lastBombx = -1;
              player.lastBomby = -1;
            }
            it = bombs.erase(it);
            Mix_PlayChannel( -1, media.getSound(Media::SoundExplosion), 0 );
            l.doExplosion(it->y, it->x);
            if (abs(playertilex - it->x) <= 1 &&
                abs(playertiley - it->y) <= 1) {
              gameState = STATE_LOST;
            }
            for (auto& m : monsters) {
              if (!m.sleeping) {
                int mtilex = (m.pos.x + m.pos.w/2) / TILE_WIDTH;
                int mtiley = (m.pos.y + m.pos.h/2) / TILE_HEIGHT;
                if (abs(mtilex - it->x) <= 1 &&
                    abs(mtiley - it->y) <= 1) {
                  m.respawn();
                  m.gotoSleep(std::chrono::seconds(5));
                }
              }
            }
          } else {
            ++it;
          }
        }

        if (l.doRepairs()) {
          l.computeDirToPlayer(player.getTileRow(), player.getTileCol(), true);
        }

        // Check if player dies:
        std::string letters = l.mapLetters(player.pos, 'X');
        if (letters.find('X') != std::string::npos) {
          gameState = STATE_LOST;
        }

        // Check if monster dies:
        for (auto& m : monsters) {
          letters = l.mapLetters(m.pos, 'X');
          if (letters.find('X') != std::string::npos) {
            m.respawn();
            m.gotoSleep(std::chrono::seconds(5));
          }
        }

        // Check if a monster got the player:
        for (auto const& m : monsters) {
          if (!m.sleeping) {
            if (checkCollision(m.pos, player.pos)) {
              gameState = STATE_LOST;
            }
          }
        }
      }

      // Finally, render image:
      auto beforeRenderCopy = std::chrono::steady_clock::now();
      SDL_RenderCopy(gRenderer, media.getBackground(), nullptr, nullptr);
      for (size_t i = 0; i < golds.size(); ++i) {
        Level::Pos const& g = golds[i];
        media.getShape(Media::ShapeGold)->render(camera, g.c * TILE_WIDTH, g.r * TILE_HEIGHT);
      }
      for (size_t i = 0; i < bombs.size(); ++i) {
        Bomb const& b = bombs[i];
        bool boom = std::chrono::duration_cast<std::chrono::duration<double>>
                    (std::chrono::steady_clock::now() - b.startTime).count() > 1;
        size_t image = boom ? Media::ShapeExplosion : Media::ShapeBomb;
        auto* explo = media.getShape(image);
        explo->render(camera, b.x * TILE_WIDTH, b.y * TILE_HEIGHT);
        if (boom) {
          if (b.x > 0) {
            explo->render(camera, (b.x-1) * TILE_WIDTH, b.y * TILE_HEIGHT);
          }
          if (b.y > 0) {
            explo->render(camera, b.x * TILE_WIDTH, (b.y-1) * TILE_HEIGHT);
          }
          if (b.x < l.cols()-1) {
            explo->render(camera, (b.x+1) * TILE_WIDTH, b.y * TILE_HEIGHT);
          }
          if (b.y < l.rows()-1) {
            explo->render(camera, b.x * TILE_WIDTH, (b.y+1) * TILE_HEIGHT);
          }
        }

      }
      auto const& repairs = l.getRepairs();
      for (auto const& rep : repairs) {
        media.getTile(Media::TileEmpty)->render(camera, rep.c * TILE_WIDTH,
                                                        rep.r * TILE_HEIGHT);
      }
      for (auto& m : monsters) {
        m.render(media, camera);
      }
      media.getShape(Media::ShapePlayer)->render(camera, static_cast<int>(player.x),
                                 static_cast<int>(player.y));
      auto beforeRenderPresent = std::chrono::steady_clock::now();
      if (gameState == STATE_LOST) {
        media.getShape(Media::ShapeGameOver)->render(camera, 325, 347);
      } else if (gameState == STATE_WON) {
      }
      SDL_RenderPresent(gRenderer);

      int playertilex = player.getTileCol();
      int playertiley = player.getTileRow();
      // Check door:
      if (gameState == STATE_PLAY &&
          golds.empty() && l.map[playertiley][playertilex] == 'D') {
        gameState = STATE_WON;
      }

      auto durSoFar = std::chrono::steady_clock::now() - frameStartTime;
#if 0
      std::cout << "Frame " << micros(beforeRenderCopy - frameStartTime)
                << " " << micros(beforeRenderPresent - frameStartTime)
                << " " << micros(durSoFar) << std::endl;
#endif
      if (durSoFar < frameTime) {
        std::this_thread::sleep_for(frameTime - durSoFar);
      }
    }
  } while (gameState != STATE_QUIT);

  if (joystick != nullptr) {
    SDL_JoystickClose(joystick);
  }
  std::cout << "Good bye!" << std::endl;
  return 0;
}
