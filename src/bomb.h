// bomb.h

#ifndef BOMB_H
#define BOMB_H 1

#include <vector>
#include <string>
#include <chrono>

struct Bomb {
  Bomb(int x, int y) 
    : x(x), y(y), startTime(std::chrono::steady_clock::now()) {
  }

  int x;
  int y;
  std::chrono::steady_clock::time_point startTime;
};

#endif
