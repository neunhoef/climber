// levels.h

#include <stdlib.h>

#ifndef LEVELS_H
#define LEVELS_H

struct LevelData {
  unsigned char* start;
  size_t len;
  LevelData(unsigned char* s, unsigned int l)
    : start(s), len(l) {}
};

extern LevelData levelData[];
#endif
