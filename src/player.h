// player.h

#ifndef PLAYER_H
#define PLAYER_H

#include "graphics.h"
#include "media.h"
#include "level.h"

class Player {
 public:
  static constexpr double speed = 0.7;
  double x;
  double y;
  double vx;
  double vy;
  int want_vx;
  int want_vy;
  SDL_Rect pos;
  int lastKeyDown;
  int lastBombx;
  int lastBomby;

  Player(int startCol, int startRow)
    : x(startCol * TILE_WIDTH), y(startRow * TILE_HEIGHT), vx(0), vy(0),
      want_vx(0), want_vy(0), lastKeyDown(0), lastBombx(-1), lastBomby(-1) {
    pos = { static_cast<int>(floor(x + 0.5)),
            static_cast<int>(floor(y + 0.5)),
            PLAYER_WIDTH, PLAYER_HEIGHT };
  }

  void move(Level& l);

  int getTileCol() {
    return (pos.x + (pos.w / 2)) / TILE_WIDTH;
  }

  int getTileRow() {
    return (pos.y + (pos.h / 2)) / TILE_HEIGHT;
  }

  void checkGold(std::vector<Level::Pos>& golds, Media& media);
};

#endif
