// player.cpp

#include "graphics.h"
#include "level.h"
#include "player.h"

void Player::move(Level& l) {
  // Compute speed of player:
  // First find out if we fall:
  int dx;
  int dy;
  Level::Direction d = l.feelsGravity(pos, dx, dy);

  if (d != Level::DirNone && (dx != 0 || dy != 0)) {
    vx = speed * dx;
    vy = speed * dy;
  } else {
    int pdx = want_vx;
    int pdy = want_vy;
    if (d == Level::DirUp || d == Level::DirDown) {
      // Vertical gravity, player can only move horizontally:
      pdy = 0;
    } else if (d == Level::DirLeft || d == Level::DirRight) {
      // Horizontal gravity, player can only move vertically:
      pdx = 0;
    }
    if (pdx != 0 || pdy != 0) {
      SDL_Rect newPos = pos;
      newPos.x += pdx;
      newPos.y += pdy;
      if (!l.positionPossible(newPos)) {
        pdx = 0;
        pdy = 0;
      }
    }
    vx = speed * pdx;
    vy = speed * pdy;
  }

  x += vx;
  y += vy;
  pos.x = static_cast<int>(floor(x + 0.5));
  pos.y = static_cast<int>(floor(y + 0.5));
}

void Player::checkGold(std::vector<Level::Pos>& golds, Media& media) {
  // Check gold pieces:
  int playertilex = getTileCol();
  int playertiley = getTileRow();
  for (auto it = golds.begin(); it != golds.end(); ) {
    Level::Pos const& g = *it;
    if (g.c == playertilex && g.r == playertiley) {
      it = golds.erase(it);
      Mix_PlayChannel( -1, media.getSound(Media::SoundPling), 0 );
    } else {
      ++it;
    }
  }
}

