// level.h

#ifndef LEVEL_H
#define LEVEL_H 1

#include <vector>
#include <deque>
#include <string>
#include <sstream>
#include <algorithm>
#include <chrono>

#include "graphics.h"

class Level {
 public:
  typedef int Direction;
  static constexpr Direction DirUp = 0;
  static constexpr Direction DirRight = 1;
  static constexpr Direction DirDown = 2;
  static constexpr Direction DirLeft = 3;
  static constexpr Direction DirNone = 4;
  static constexpr Direction DirUndiscovered = 5;
    // only used in shortest path to player search
    
  struct Pos {
    int r;  // row, zero based
    int c;  // col, zero based
    Pos(int rr, int cc) : r(rr), c(cc) {
    }
  };

  static std::array<Pos, 5> const neighbors;

 private:
  size_t _rows;
  size_t _cols;
  std::vector<Pos> _golds;

 public:
  std::vector<std::string> map;   // has _rows strings of length _cols
  Direction* gravity;             // two-dimensional array
  size_t startRow;
  size_t startCol;

  Level() : _rows(0), _cols(0), gravity(nullptr), startRow(0), startCol(0),
            lastr(-1), lastc(-1) {
  }

  ~Level() {
    delete[] gravity;
  }

  void clear() {
    delete[] gravity;
    map.clear();
    _rows = 0;
    _cols = 0;
    _golds.clear();
  }

  size_t rows() const {
    return _rows;
  }

  size_t cols() const {
    return _cols;
  }

  std::vector<Pos> golds() const {
    return _golds;
  }

  bool load(long nr);

  void show();

  std::string mapLetters(SDL_Rect& obj, char stopIf);

  bool isInMap(SDL_Rect& obj) {
    if (obj.x < 0 || obj.y < 0 ||
        obj.x + obj.w > SCREEN_WIDTH || obj.y + obj.h > SCREEN_HEIGHT) {
      return false;
    }
    return true;
  }

  bool positionPossible(SDL_Rect& obj) {
    // Returns true if an object can be at the position described by the
    // rectangle. The object cannot be if it overlaps with a block or if it
    // reaches outside the grid.
    if (!isInMap(obj)) {
      return false;
    }
    std::string found = mapLetters(obj, 'X');
    if (found.find('X') != std::string::npos) {
      return false;
    }
    return true;
  }

  Direction feelsGravity(SDL_Rect& obj, int& dx, int& dy);
    // Returns a direction if the center of the object is on a tile with
    // gravity in that direction, or DirNone, if there is no gravity
    // here. If the object can move one pixel in this direction, then dx and
    // dy are set accordingly, otherwise they are set to 0. This also happens
    // if there is no gravity. Note that obj must be within the screen!

  void doExplosion(int r, int c) {
    // Does an explosion in the square at row r and column c. This means that
    // everything up, down, left and right is temporarily removed. Things come
    // back after a few seconds.
    std::array<Pos, 5> const neighbours
      {Pos(0,0), Pos(-1,0),Pos(0,1),Pos(1,0),Pos(0,-1)};
    for (auto const& p : neighbours) {
      int rr = r + p.r;
      int cc = c + p.c;
      if (rr >= 0 && cc >= 0 && rr < _rows && cc < _cols) {
        if (map[rr][cc] != ' ') {
          _repairs.emplace_back(rr, cc, map[rr][cc]);
          map[rr][cc] = ' ';
        }
      }
    }
  }
 
  bool doRepairs() {
    bool worked = false;
    while (true) {
      if (_repairs.empty()) {
        return worked;
      }
      worked = true;

      auto const& rep = _repairs.front();
      auto now = std::chrono::steady_clock::now();
      if (now > rep.end) {
        map[rep.r][rep.c] = rep.what;
        _repairs.pop_front();
      } else {
        break;
      }
    }
    return worked;
  }

  struct Repair {
    int r;      // row
    int c;      // column
    char what;  // block to repair at map[r][c]
    std::chrono::steady_clock::time_point end;
    Repair(int rr, int cc, char w) 
      : r(rr), c(cc), what(w), end(std::chrono::steady_clock::now() +
                                   std::chrono::seconds(5)) {
    }
  };

  std::deque<Repair> const& getRepairs() const {
    return _repairs;
  }

  std::vector<std::vector<Direction>> dirToPlayer;
  int lastr;
  int lastc;

  void computeDirToPlayer(int r, int c, bool force);
  // Computes for every grid position that does not contain a block the
  // direction for the shortest path to the player.

  std::vector<Pos> monsters;

 private:
  std::deque<Repair> _repairs;
};

#endif
