// media.cpp

#include <iostream>

#include "graphics.h"
#include "media.h"

#include "files.h"

bool Media::init(bool fullscreen) {
  if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
    std::cerr << "Could not initialize SDL, error: " << SDL_GetError()
      << std::endl;
    return false;
  }
  _nrJoy = SDL_NumJoysticks();
  if (_nrJoy > 0) {
    std::cout << "Joysticks found: " << _nrJoy << std::endl;
  }
  if(Mix_OpenAudio( 44000, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) {
    SDL_Quit();
    return false;
  }
  if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1")) {
    std::cerr << "Linear texture filtering not enabled!" << std::endl;
  }
  gWindow = SDL_CreateWindow("Climber", SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
  if (gWindow == nullptr) {
    std::cerr << "Window could not be created! SDL Error: " << SDL_GetError()
      << std::endl;
    return false;
  }
  if (fullscreen) {
    SDL_SetWindowFullscreen(gWindow, SDL_WINDOW_FULLSCREEN);
  }
  gRenderer = SDL_CreateRenderer( gWindow, -1,
      SDL_RENDERER_ACCELERATED /* | SDL_RENDERER_PRESENTVSYNC */ );
  if (gRenderer == nullptr) {
    std::cerr << "Renderer could not be created! SDL Error: "
      << SDL_GetError() << std::endl;
    return false;
  }
  SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
  return true;
}

Media::~Media() {
  if (_background != nullptr) {
    SDL_DestroyTexture(_background);
    _background = nullptr;
  }
    
  for (auto* s : _sounds) {
    Mix_FreeChunk(s);
  }
  _sounds.clear();
  for (size_t i = 0; i < _tiles.size(); ++i) {
    _tiles[i].reset(nullptr);
  }
  for (size_t i = 0; i < _shapes.size(); ++i) {
    _shapes[i].reset(nullptr);
  }

  if (gRenderer != nullptr) {
    SDL_DestroyRenderer(gRenderer);
    gRenderer = nullptr;
  }
  if (gWindow != nullptr) {
    SDL_DestroyWindow(gWindow);
    gWindow = nullptr;
  }
  SDL_Quit();
}

bool Media::loadMedia() {
  _tiles.clear();
  std::vector<std::string> filenames = {
    "up.bmp", "right.bmp", "down.bmp", "left.bmp", "empty.bmp",
    "block.bmp", "ladder.bmp", "door.bmp"
  };
  for (auto const& n : filenames) {
    auto nextTile = std::make_unique<Tile>();
    if (!nextTile->loadFromFile("./" + n, gWindow)) {
      std::cerr << "Could not load '" << n << "' image" << std::endl;
      return false;
    }
    _tiles.emplace_back(std::move(nextTile));
  }
  _shapes.clear();
  filenames = {
    "player.bmp", "gold.bmp", "bomb.bmp", "explosion.bmp",
    "monster.bmp", "monster2.bmp", "monster3.bmp", "gameover.bmp"
  };
  for (auto const& n : filenames) {
    auto nextShape = std::make_unique<Tile>();
    if (!nextShape->loadFromFile("./" + n, gWindow)) {
      std::cerr << "Could not load image '" << n << "'." << std::endl;
      return false;
    }
    _shapes.emplace_back(std::move(nextShape));
  }
  filenames = {
    "palimpalim.wav", "explosion.wav"
  };
  for (auto const& n : filenames) {
    Mix_Chunk* s = Mix_LoadWAV(("./" + n).c_str());
    if (s == nullptr) {
      return false;
    }
    _sounds.push_back(s);
  }
  return true;
}

bool Media::loadMediaFromMemory() {
  _tiles.clear();
  std::vector<std::pair<unsigned char*, unsigned int>> data = {
    std::make_pair(up_bmp, up_bmp_len),
    std::make_pair(right_bmp, right_bmp_len),
    std::make_pair(down_bmp, down_bmp_len),
    std::make_pair(left_bmp, left_bmp_len),
    std::make_pair(empty_bmp, empty_bmp_len),
    std::make_pair(block_bmp, block_bmp_len),
    std::make_pair(ladder_bmp, ladder_bmp_len),
    std::make_pair(door_bmp, door_bmp_len)
  };
  for (auto const& d : data) {
    auto nextTile = std::make_unique<Tile>();
    if (!nextTile->loadFromMemory(d.first, d.second, gWindow)) {
      return false;
    }
    _tiles.emplace_back(std::move(nextTile));
  }
  _shapes.clear();
  data = {
    std::make_pair(player_bmp, player_bmp_len),
    std::make_pair(gold_bmp, gold_bmp_len),
    std::make_pair(bomb_bmp, bomb_bmp_len),
    std::make_pair(explosion_bmp, explosion_bmp_len),
    std::make_pair(monster_bmp, monster_bmp_len),
    std::make_pair(monster2_bmp, monster2_bmp_len),
    std::make_pair(monster3_bmp, monster3_bmp_len),
    std::make_pair(gameover_bmp, gameover_bmp_len)
  };

  for (auto const& d : data) {
    auto nextShape = std::make_unique<Tile>();
    if (!nextShape->loadFromMemory(d.first, d.second, gWindow)) {
      return false;
    }
    _shapes.emplace_back(std::move(nextShape));
  }
  data= {
    std::make_pair(palimpalim_wav, palimpalim_wav_len),
    std::make_pair(explosion_wav, explosion_wav_len)
  };
  for (auto const& d : data) {
    SDL_RWops* rwops = SDL_RWFromConstMem(d.first, static_cast<int>(d.second));
    Mix_Chunk* s = Mix_LoadWAV_RW(rwops, 1);
    if (s == nullptr) {
      return false;
    }
    _sounds.push_back(s);
  }
  return true;
}

void Media::makeBackground(Level const& l) {
  SDL_Surface* background = SDL_CreateRGBSurface(0, l.cols() * TILE_WIDTH,
                                                    l.rows() * TILE_HEIGHT,
                                                    32, 0, 0, 0, 0);
  for (int x = 0; x < l.cols(); ++x) {
    for (int y = 0; y < l.rows(); ++y) {
      if (l.map[y][x] == ' ') {
        _tiles[l.gravity[y * l.cols() + x]]->blit(
            x * TILE_WIDTH, y * TILE_HEIGHT, background);
      } else if (l.map[y][x] == '#') {
        _tiles[6]->blit(x * TILE_WIDTH, y * TILE_HEIGHT, background);
      } else if (l.map[y][x] == 'X') {
        _tiles[5]->blit(x * TILE_WIDTH, y * TILE_HEIGHT, background);
      } else if (l.map[y][x] == 'D') {
        _tiles[7]->blit(x * TILE_WIDTH, y * TILE_HEIGHT, background);
      }
    }
  }
  _background = SDL_CreateTextureFromSurface(gRenderer, background);
  SDL_FreeSurface(background);
}

