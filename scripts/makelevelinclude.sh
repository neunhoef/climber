#!/bin/bash

TARGET=../src/levels.cpp

rm -f src/levels.cpp
cd levels
echo '#include <stdlib.h>' > $TARGET
echo '#include "levels.h"' > $TARGET
for f in * ; do
  xxd -i $f >> $TARGET
done

echo "LevelData levelData[] = {" >> $TARGET

for f in * ; do
  echo "LevelData("$f", "$f"_len)," >> $TARGET
done

echo "LevelData(nullptr, 0)" >> $TARGET
echo "};" >> $TARGET
