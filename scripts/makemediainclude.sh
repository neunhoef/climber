#!/bin/bash

TARGET=../src/files.h

cd images
rm -f $TARGET
for f in up.bmp right.bmp down.bmp left.bmp empty.bmp block.bmp ladder.bmp door.bmp ; do
  xxd -i $f >> $TARGET
done

for f in player.bmp gold.bmp bomb.bmp explosion.bmp monster.bmp monster2.bmp monster3.bmp gameover.bmp ; do
  xxd -i $f >> $TARGET
done

cd ../sounds

for f in palimpalim.wav explosion.wav ; do
  xxd -i $f >> $TARGET
done
