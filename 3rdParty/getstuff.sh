#!/bin/sh
if test ! -d SDL2Test ; then
    git clone https://github.com/trenki2/SDL2Test
fi
if test ! -d SDL2_mixer_test ; then
    mkdir SDL2_mixer_test
    cd SDL2_mixer_test
    wget https://raw.githubusercontent.com/Tangent128/luasdl2/master/cmake/FindSDL2_mixer.cmake
fi
